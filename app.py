from datetime import datetime
from flask import Flask
from flask_restful import reqparse, abort, Api, Resource

app = Flask(__name__)
api = Api(app)

TODOS = {
    1: {'task': 'build an API'},
    2: {'task': '中文測試'},
    3: {'task': 'profit!'},
}


def abort_if_todo_doesnt_exist(todo_id):
    if todo_id not in TODOS:
        abort(404, message=f"Todo {todo_id} doesn't exist")


parser = reqparse.RequestParser()
parser.add_argument('task', type=str, default = "")
parser.add_argument('name', type=str, default = "")
parser.add_argument('date', type=lambda x: datetime.strptime(x,'%Y-%m-%d'), default = "")


# Todo
# shows a single todo item and lets you delete a todo item
class Todo(Resource):
    def get(self, todo_id):
        print('todo_id: ', todo_id)
        abort_if_todo_doesnt_exist(todo_id)
        return TODOS[todo_id]

    def delete(self, todo_id):
        abort_if_todo_doesnt_exist(todo_id)
        del TODOS[todo_id]
        return '', 204

    def put(self, todo_id):
        args = parser.parse_args()
        task = {'task': args['task']}
        TODOS[todo_id] = task
        return task, 201


# TodoList
# shows a list of all todos, and lets you POST to add new tasks
class TodoList(Resource):
    # def __init__(self):
    #     self.reqparse = reqparse.RequestParser()
    #     self.reqparse.add_argument('task', type=str, default = "", location='args')

    def get(self):
        # args = self.reqparse.parse_args()
        # task_content = args["task"]
        # print(task_content)

        return TODOS

    def post(self):
        args = parser.parse_args()

        print(args)

        todo_id = max(TODOS.keys()) + 1
        TODOS[todo_id] = {'task': args['task']}
        return TODOS[todo_id], 201



##
# Actually setup the Api resource routing here
##
api.add_resource(TodoList, '/todos')
api.add_resource(Todo, '/todos/<int:todo_id>')


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000, debug=True)
